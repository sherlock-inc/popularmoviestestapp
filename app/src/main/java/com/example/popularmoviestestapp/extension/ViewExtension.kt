package com.example.popularmoviestestapp.extension

import com.whinc.widget.ratingbar.RatingBar
import kotlin.math.ceil

fun RatingBar.calculateRatingBarCount(voteAverage: Float) {
    count = if (voteAverage == 0.0f) 0 else ceil((ceil(voteAverage) + 10) / maxCount).toInt()
}
