package com.example.popularmoviestestapp.extension

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.reflect.TypeToken

inline fun <reified T> Gson.fromJson(json: JsonArray?) =
    fromJson<T>(json, object : TypeToken<T>() {}.type)