package com.example.popularmoviestestapp.network

import com.example.popularmoviestestapp.BuildConfig
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RestClient {

    private const val baseUrl = "https://api.themoviedb.org/3/"

    private val loggingInterceptor = with(HttpLoggingInterceptor()) {
        level = HttpLoggingInterceptor.Level.BODY
        this
    }

    private val client = with(OkHttpClient.Builder()) {
        if (BuildConfig.DEBUG) {
            addInterceptor(loggingInterceptor)
        }

        addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val original: Request = chain.request()
                val originalHttpUrl: HttpUrl = original.url

                val url = with(originalHttpUrl.newBuilder()) {
                    addQueryParameter("api_key", "9a9b70ed0e6b06aa6e70cf752c0a0c80")
                    addQueryParameter("language", "en-US")
                    build()
                }
                val request = original.newBuilder().url(url).build()
                return chain.proceed(request)
            }

        })
        build()
    }

    val retrofit: Retrofit = with(Retrofit.Builder()) {
        baseUrl(baseUrl)
        addConverterFactory(GsonConverterFactory.create())
        client(client)
        build()
    }
}