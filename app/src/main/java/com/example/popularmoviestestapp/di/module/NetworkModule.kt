package com.example.popularmoviestestapp.di.module

import com.example.popularmoviestestapp.network.MovieService
import com.example.popularmoviestestapp.network.RestClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object NetworkModule {

    @JvmStatic
    @Singleton
    @Provides
    fun providesMovieService(): MovieService = RestClient.retrofit.create(MovieService::class.java)
}