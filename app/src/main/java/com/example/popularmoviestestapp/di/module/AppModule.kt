package com.example.popularmoviestestapp.di.module

import com.example.popularmoviestestapp.movie.di.ActivityBindingModule
import dagger.Module

@Module(includes = [NetworkModule::class, ActivityBindingModule::class])
class AppModule