package com.example.popularmoviestestapp

import com.example.popularmoviestestapp.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class PopularMoviesApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val component = DaggerAppComponent.builder().applicationContext(this).build()
        component.inject(this)
        return component
    }
}