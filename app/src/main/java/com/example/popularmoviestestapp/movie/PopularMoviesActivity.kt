package com.example.popularmoviestestapp.movie

import android.net.NetworkInfo
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.popularmoviestestapp.BaseActivity
import com.example.popularmoviestestapp.R
import com.example.popularmoviestestapp.extension.injectViewModel
import com.example.popularmoviestestapp.movie.adapter.PopularMovieAdapter
import com.example.popularmoviestestapp.movie.adapter.SpaceItemDecorator
import com.example.popularmoviestestapp.movie.data.ConnectedState
import kotlinx.android.synthetic.main.popular_movies_activity.*
import javax.inject.Inject

class PopularMoviesActivity : BaseActivity() {

    private val viewModel: PopularMoviesViewModel by lazy {
        injectViewModel<PopularMoviesViewModel>(viewModelFactory)
    }
    val itemDecorationSpace by lazy { resources.getDimensionPixelOffset(R.dimen.recycler_view_item_space) }

    @Inject
    lateinit var popularMovieAdapter: PopularMovieAdapter

    @Inject
    lateinit var linearLayoutManager: LinearLayoutManager

    @Inject
    lateinit var spaceItemDecorator: SpaceItemDecorator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.popular_movies_activity)
        initRecyclerView()
        observes()
    }

    private fun initRecyclerView() {
        recyclerView.apply {
            layoutManager = linearLayoutManager
            addItemDecoration(spaceItemDecorator)
            adapter = popularMovieAdapter
        }
    }

    private fun observes() {
        viewModel.popularMoviesLiveData.observe(this, Observer {
            popularMovieAdapter.submitList(it.first)
            val visibility = if (it.second.state() == NetworkInfo.State.CONNECTED) View.GONE else View.VISIBLE
            tvConnectivityTitle.visibility = visibility
            tvConnectivityDesc.visibility = visibility
        })
    }
}