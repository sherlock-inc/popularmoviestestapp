package com.example.popularmoviestestapp.movie.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.popularmoviestestapp.di.util.ActivityScope
import com.example.popularmoviestestapp.movie.data.model.Movie
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@ActivityScope
class PopularMoviesDataSourceFactory @Inject constructor(
    private val repository: MovieRepository,
    private val compositeDisposable: CompositeDisposable
) : DataSource.Factory<Int, Movie>() {

    val sourceLiveData = MutableLiveData<PopularMoviesDataSource>()

    override fun create(): DataSource<Int, Movie> {
        val dataSource = PopularMoviesDataSource(repository, compositeDisposable)
        sourceLiveData.postValue(dataSource)
        return dataSource
    }
}