package com.example.popularmoviestestapp.movie.data

sealed class NetworkState

object ConnectedState : NetworkState()
object DisconnectedState : NetworkState()

