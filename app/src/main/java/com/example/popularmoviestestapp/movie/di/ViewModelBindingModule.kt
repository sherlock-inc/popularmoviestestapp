package com.example.popularmoviestestapp.movie.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.popularmoviestestapp.ViewModelFactory
import com.example.popularmoviestestapp.di.util.ActivityScope
import com.example.popularmoviestestapp.di.util.ViewModelKey
import com.example.popularmoviestestapp.movie.PopularMoviesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
abstract class ViewModelBindingModule {

    @Binds
    @ActivityScope
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ActivityScope
    @ViewModelKey(PopularMoviesViewModel::class)
    internal abstract fun  popularMoviesViewModel(viewModel: PopularMoviesViewModel): ViewModel
}