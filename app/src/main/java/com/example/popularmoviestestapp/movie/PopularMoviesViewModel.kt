package com.example.popularmoviestestapp.movie

import android.net.NetworkInfo
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import androidx.paging.RxPagedListBuilder
import com.example.popularmoviestestapp.movie.data.ConnectedState
import com.example.popularmoviestestapp.movie.data.DisconnectedState
import com.example.popularmoviestestapp.movie.data.NetworkState
import com.example.popularmoviestestapp.movie.data.PopularMoviesDataSourceFactory
import com.example.popularmoviestestapp.movie.data.model.Movie
import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.lang.RuntimeException
import javax.inject.Inject

class PopularMoviesViewModel @Inject constructor(
    private val connectivityObservable: Observable<Connectivity>,
    private val dataSourceFactory: PopularMoviesDataSourceFactory,
    private val compositeDisposable: CompositeDisposable
) : ViewModel() {

    var popularMoviesLiveData = MutableLiveData<Pair<PagedList<Movie>?, Connectivity>>()

    init {
        val config = PagedList.Config.Builder()
            .setPageSize(PAGE_SIZE)
            .setInitialLoadSizeHint(PAGE_SIZE)
            .setEnablePlaceholders(true).build()

        val pagedListObservable = RxPagedListBuilder(dataSourceFactory, config)
            .setFetchScheduler(Schedulers.io())
            .buildObservable()

        compositeDisposable.addAll(connectivityObservable.flatMap {
            Observable.zip(pagedListObservable, Observable.just(it),
                BiFunction<PagedList<Movie>, Connectivity, Pair<PagedList<Movie>?, Connectivity>> { pagedList, connectivity ->
                    pagedList to connectivity
                })
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                popularMoviesLiveData.value = it
            })
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    companion object {
        const val PAGE_SIZE = 20
    }
}