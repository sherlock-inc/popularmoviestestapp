package com.example.popularmoviestestapp.movie.data.model

data class Movie(
    val voteAverage: Float,
    val posterPath: String,
    val title: String,
    val overview: String
)