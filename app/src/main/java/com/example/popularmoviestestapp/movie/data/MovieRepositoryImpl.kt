package com.example.popularmoviestestapp.movie.data

import com.example.popularmoviestestapp.extension.fromJson
import com.example.popularmoviestestapp.movie.data.model.Movie
import com.example.popularmoviestestapp.movie.data.model.MovieDto
import com.example.popularmoviestestapp.movie.data.model.adapter.MovieAdapter
import com.example.popularmoviestestapp.network.MovieService
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(private val service: MovieService) : MovieRepository {
    override fun getPopularMovies(page: Int): Single<List<Movie>> {
        return Single.create { emitter ->
            service.getPopularMovies(page).enqueue(object : Callback<JsonObject> {
                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    emitter.onError(t)
                }

                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    val jsonResponse = response.body()?.getAsJsonArray("results")
                    val dtoResponse =  Gson().fromJson<List<MovieDto>>(jsonResponse)
                    emitter.onSuccess(MovieAdapter.fromResponse(dtoResponse))
                }
            })
        }
    }
}