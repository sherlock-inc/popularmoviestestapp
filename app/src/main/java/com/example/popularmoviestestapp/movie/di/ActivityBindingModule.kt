package com.example.popularmoviestestapp.movie.di

import com.example.popularmoviestestapp.di.util.ActivityScope
import com.example.popularmoviestestapp.movie.PopularMoviesActivity
import com.example.popularmoviestestapp.movie.di.PopularMoviesActivityModule
import com.example.popularmoviestestapp.movie.di.ViewModelBindingModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [ViewModelBindingModule::class, PopularMoviesActivityModule::class])
    abstract fun popularMoviesActivity(): PopularMoviesActivity
}