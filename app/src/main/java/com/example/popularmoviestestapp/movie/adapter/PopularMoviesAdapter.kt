package com.example.popularmoviestestapp.movie.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.popularmoviestestapp.R
import com.example.popularmoviestestapp.extension.calculateRatingBarCount
import com.example.popularmoviestestapp.movie.data.model.Movie
import kotlinx.android.synthetic.main.popular_movie_item_layout.view.*
import javax.inject.Inject

class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(movie: Movie) {
        with(itemView) {
            tvTitle.text = movie.title
            tvDescription.text = movie.overview
            tvRating.text = movie.voteAverage.toString()
            ratingBar.calculateRatingBarCount(movie.voteAverage)
            Glide.with(context)
                .load(context.getString(R.string.image_path, movie.posterPath))
                .into(ivLogo)
        }
    }
}

class PopularMovieAdapter @Inject constructor(callback: PaginationItemCallback) :
    PagedListAdapter<Movie, MovieViewHolder>(callback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.popular_movie_item_layout, parent, false)
        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = getItem(position)
        movie?.let { holder.bind(it) }
    }
}

object PaginationItemCallback: DiffUtil.ItemCallback<Movie>() {

    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.voteAverage == newItem.voteAverage && oldItem.posterPath == newItem.posterPath
                && oldItem.title == newItem.title && oldItem.overview == newItem.overview
    }
}