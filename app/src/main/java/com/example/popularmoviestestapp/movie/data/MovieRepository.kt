package com.example.popularmoviestestapp.movie.data

import com.example.popularmoviestestapp.movie.data.model.Movie
import io.reactivex.Single

interface MovieRepository {
    fun getPopularMovies(page: Int): Single<List<Movie>>
}