package com.example.popularmoviestestapp.movie.di

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.popularmoviestestapp.di.util.ActivityScope
import com.example.popularmoviestestapp.movie.PopularMoviesActivity
import com.example.popularmoviestestapp.movie.adapter.PaginationItemCallback
import com.example.popularmoviestestapp.movie.adapter.PopularMovieAdapter
import com.example.popularmoviestestapp.movie.adapter.SpaceItemDecorator
import com.example.popularmoviestestapp.movie.data.MovieRepository
import com.example.popularmoviestestapp.movie.data.MovieRepositoryImpl
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import dagger.Binds
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
abstract class PopularMoviesActivityModule {

    @Binds
    @ActivityScope
    abstract fun bindsMovieRepositoryDecorator(repository: MovieRepositoryImpl): MovieRepository

    @Module
    companion object {

        @Provides
        @ActivityScope
        @JvmStatic
        fun provideConnectivityObservable(context: Context) = ReactiveNetwork
            .observeNetworkConnectivity(context)

        @Provides
        @ActivityScope
        @JvmStatic
        fun providePopularMoviesAdapter() = PopularMovieAdapter(PaginationItemCallback)

        @Provides
        @ActivityScope
        @JvmStatic
        fun provideVerticalLinearLayout(activity: PopularMoviesActivity) =
            LinearLayoutManager(activity)

        @Provides
        @ActivityScope
        @JvmStatic
        fun provideSpaceItemDecorator(activity: PopularMoviesActivity) =
            SpaceItemDecorator(activity.itemDecorationSpace)


        @JvmStatic
        @ActivityScope
        @Provides
        fun provideCompositeDisposable() = CompositeDisposable()
    }
}