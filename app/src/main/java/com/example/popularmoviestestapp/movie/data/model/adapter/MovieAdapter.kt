package com.example.popularmoviestestapp.movie.data.model.adapter

import com.example.popularmoviestestapp.movie.data.model.Movie
import com.example.popularmoviestestapp.movie.data.model.MovieDto

object MovieAdapter {
    fun fromResponse(list: List<MovieDto>) : List<Movie> =
        list.map { Movie(it.voteAverage, it.posterPath, it.title, it.overview) }
}